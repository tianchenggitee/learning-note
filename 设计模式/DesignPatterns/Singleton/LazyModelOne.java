package com.example.app.DesignPatterns.Singleton;

/**
 * @author tian
 * @item 2022/11/10 20:13:53
 * 懒汉模式
 */
public class LazyModelOne {
    private volatile static LazyModelOne modelOne = null;

    private LazyModelOne() {
    }

    public LazyModelOne getModelOne(LazyModelOne modelOne) {
        if (modelOne == null) {
            synchronized (this) {
                if (modelOne == null) {
                    modelOne = new LazyModelOne();
                }
            }
        }
        return modelOne;
    }

}
