package com.example.app.DesignPatterns.Singleton;

/**
 * @author tian
 * @item 2022/11/10 20:10:56
 * 恶汉模式
 */

public class VillainModel {
    private static VillainModel villainModel = new VillainModel();

    public VillainModel() {
    }

    private VillainModel getVillainModel(VillainModel villainModel) {
        return villainModel;
    }
}
