package com.example.app.DesignPatterns.agent.dinamicProxyDemo;

/**
 * @author tian
 * @item 2022/11/14 15:22:14
 */
public interface BaseService {
    void mainService();
}

class Cicada implements BaseService {

    @Override
    public void mainService() {
        System.out.println("当禅出现业务时，螳螂监听到");
    }
}