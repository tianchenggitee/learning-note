package com.example.app.DesignPatterns.agent.dinamicProxyDemo;

import istio.v1.auth.Ca;
import org.springframework.cglib.proxy.InvocationHandler;
import org.springframework.cglib.proxy.Proxy;

/**
 * @author tian
 * @item 2022/11/14 15:31:28
 */
public class BeanFactory {
    public static BaseService newInstance(Class classFile) {
        //创建禅  真实对象
        BaseService trueCicada = new Cicada();

        //创建代理对象 螳螂
        InvocationHandler prayingMantis = new PrayingMantis((trueCicada));

        //向jvm索要代理对象  其实就是监听对象
        Class classArray[] = {BaseService.class};

        BaseService baseService = (BaseService) Proxy.newProxyInstance(classFile.getClassLoader(), classArray, prayingMantis);

        return baseService;
    }
    //测试demo
    public static void main(String[] args) {
        BaseService baseService = newInstance(Cicada.class);
        baseService.mainService();

    }
}
