package com.example.app.DesignPatterns.agent.dinamicProxyDemo;

import org.springframework.cglib.proxy.InvocationHandler;

import java.lang.reflect.Method;

/**
 * @author tian
 * @item 2022/11/14 15:27:40
 */
public class PrayingMantis implements InvocationHandler {

    private BaseService baseService;
    //采用构建传参
    public PrayingMantis(BaseService baseService) {
        this.baseService = baseService;
    }

    //螳螂主要业务  也就是监听对象
    @Override
    public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
        method.invoke(baseService, objects);
        secibdaryMain();
        return null;
    }
    private void secibdaryMain(){
        System.out.println("螳螂捕蝉，次要业务");

    }
}
