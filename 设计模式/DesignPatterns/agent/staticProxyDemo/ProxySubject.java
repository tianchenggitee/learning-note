package com.example.app.DesignPatterns.agent.staticProxyDemo;

/**
 * @author tian
 * @item 2022/11/14 14:47:01
 */
public class ProxySubject implements Subject {
    private RealSubject realSubject;

    public ProxySubject(RealSubject realSubject) {
        this.realSubject = realSubject;
    }

    public ProxySubject() throws Exception {
        this.realSubject = (RealSubject) this.getClass().getClassLoader().loadClass("com.example.app.DesignPatterns.agent.staticProxyDemo.RealSubject").newInstance();
    }

    @Override
    public void dosomething() {
        realSubject.dosomething();
    }

    public static void main(String[] args) throws Exception {
        //第一种
        new ProxySubject().dosomething();
        System.out.println("-------------------------------");
        //第二种
        new ProxySubject(new RealSubject()).dosomething();

    }
}
