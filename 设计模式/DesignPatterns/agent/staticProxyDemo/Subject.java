package com.example.app.DesignPatterns.agent.staticProxyDemo;

/**
 * @author tian
 * @item 2022/11/14 14:40:46
 */
public interface Subject {
    //共同的接口
    void dosomething();
}
