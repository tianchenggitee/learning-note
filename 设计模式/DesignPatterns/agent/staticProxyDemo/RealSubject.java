package com.example.app.DesignPatterns.agent.staticProxyDemo;

/**
 * @author tian
 * @item 2022/11/14 14:41:30
 */
public class RealSubject implements Subject{
    @Override
    public void dosomething() {
        System.out.println("打游戏");
    }
}
