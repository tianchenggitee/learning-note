package com.example.app.DesignPatterns.strategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tian
 * @item 2022/11/03 17:26:01
 */
@RestController
@RequestMapping("/apply")
public class BizController {

    @Autowired
    private BizService bizService;

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test2(String order) {
        return bizService.getCheckResultSuper(order);
    }
}
