package com.example.app.DesignPatterns.proxyModel.staticProxy;

import com.example.app.DesignPatterns.proxyModel.RealSubject;
import com.example.app.DesignPatterns.proxyModel.Subject;

/**
 * @author tian
 * 静态代理
 * @item 2022/12/09 13:37:20
 */
public class staticProxy implements Subject {
    private RealSubject realSubject;

    //传入需要的构造参数
    public staticProxy(RealSubject realSubject) {
        this.realSubject = realSubject;
    }

    @Override
    public void request() {
        //静态代理类执行买房前后的动作
        System.out.println("卖房前");
        realSubject.request();
        System.out.println("卖房后");

    }
}
