package com.example.app.DesignPatterns.proxyModel.staticProxy;

import com.example.app.DesignPatterns.proxyModel.RealSubject;

/**
 * @author tian
 * 静态代理测试
 * @item 2022/12/09 13:38:34
 */
public class staticTest {
    public static void main(String[] args) {
        //创建一个目标对象
        RealSubject realSubject = new RealSubject();
        //传入静态代理对象
        staticProxy staticProxy = new staticProxy(realSubject);
        //执行方法
        staticProxy.request();

    }
}
