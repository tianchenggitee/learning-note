package com.example.app.DesignPatterns.proxyModel.dynamicProxy;

import com.example.app.DesignPatterns.proxyModel.Subject;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author tian
 * 动态代理
 * @item 2022/12/09 13:43:23
 */
public class dynamicProxy {

    private Object target;//维护一个目标对象

    public dynamicProxy(Object target) {
        this.target = target;
    }

    public Object getProxyInstance(){
        return Proxy.newProxyInstance(target.getClass().getClassLoader(), target.getClass().getInterfaces(), new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                System.out.println("卖房前");//执行逻辑的前置逻辑
                method.invoke(target, args);//执行目标对象的方法
                System.out.println("卖房后");//执行逻辑的后置逻辑
                return null;
            }
        });
    }
//    我们注意到代理类的 class 为 com.sun.proxy.$Proxy0，它是如何生成的呢，注意到 Proxy 是在 java.lang.reflect 反射包下的，注意看看 Proxy 的 newProxyInstance 签名
//
//    public static Object newProxyInstance(ClassLoader loader,
//                                          Class<?>[] interfaces,
//                                          InvocationHandler h);
//    loader: 代理类的ClassLoader，最终读取动态生成的字节码，并转成 java.lang.Class 类的一个实例（即类），通过此实例的 newInstance() 方法就可以创建出代理的对象
//    interfaces: 委托类实现的接口，JDK 动态代理要实现所有的委托类的接口
//    InvocationHandler: 委托对象所有接口方法调用都会转发到 InvocationHandler.invoke()，在 invoke() 方法里我们可以加入任何需要增强的逻辑 主要是根据委托类的接口等通过反射生成的

}
