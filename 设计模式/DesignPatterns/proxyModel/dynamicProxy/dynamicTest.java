package com.example.app.DesignPatterns.proxyModel.dynamicProxy;

import com.example.app.DesignPatterns.proxyModel.RealSubject;
import com.example.app.DesignPatterns.proxyModel.Subject;

/**
 * @author tian
 * 动态代理测试
 * @item 2022/12/09 13:53:33
 */
public class dynamicTest {
    public static void main(String[] args) {
        //创建一个目标对象
        RealSubject realSubject = new RealSubject();
        System.out.println("realSubject = " + realSubject);
        //传入目标对象获取实例
        Subject subject = (Subject) new dynamicProxy(realSubject).getProxyInstance();
        //调用代理方法
        subject.request();

    }
}
