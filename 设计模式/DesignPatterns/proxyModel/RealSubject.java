package com.example.app.DesignPatterns.proxyModel;

import com.example.app.DesignPatterns.proxyModel.Subject;

/**
 * @author tian
 * 真实代理对象
 * @item 2022/12/09 13:36:29
 */
public class RealSubject implements Subject {

    @Override
    public void request() {
        //真实对象的买房动作
        System.out.println("卖房");
    }
}
