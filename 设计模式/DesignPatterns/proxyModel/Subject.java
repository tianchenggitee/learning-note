package com.example.app.DesignPatterns.proxyModel;

/**
 * @author tian
 * 代理接口
 * @item 2022/12/09 13:36:01
 */
public interface Subject {
    public void request();
}
