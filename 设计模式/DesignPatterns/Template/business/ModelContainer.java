package com.example.app.DesignPatterns.Template.business;

/**
 * @author tian
 * @item 2022/11/14 09:59:32
 */
public class ModelContainer {
    public static void main(String[] args) {
        ItemInfoBlock itemInfoBlock = new ItemInfoBlock();
        ModelContainer modelContainer = new ModelContainer();
        ItemInfoBlock.itemInfo template = itemInfoBlock.template(modelContainer);
        System.out.println(template);
        System.out.println("-----------------------");
        SkuBlock skuBlock = new SkuBlock();
        SkuBlock.skuInfo template1 = skuBlock.template(modelContainer);
        System.out.println(template1);
        System.out.println();
        //获取cpu核数（java）
        System.out.println(Runtime.getRuntime().availableProcessors());
        //linux 下获取cpu核数  lscpu
    }
}
