package com.example.app.DesignPatterns.Template.business;

import lombok.Data;

/**
 * @author tian
 * @item 2022/11/14 10:17:00
 */
public class SkuBlock extends AbstractTemplateBlock<SkuBlock.skuInfo> {

    @Override
    protected skuInfo initBlock() {
        return new SkuBlock.skuInfo();
    }

    @Override
    protected void doWork(ModelContainer modelContainer, skuInfo block) {
        block.setItemId(456L);
        block.setItemName("SKU");
    }

    @Data
    public class skuInfo {
        private Long itemId;
        private String itemName;
    }
}
