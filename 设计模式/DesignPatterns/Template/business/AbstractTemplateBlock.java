package com.example.app.DesignPatterns.Template.business;

/**
 * @author tian
 * @item 2022/11/14 09:34:12
 */
public abstract class AbstractTemplateBlock<T> {
    // 组装结果
    public T template(ModelContainer modelContainer ){
        T block = initBlock();
        try {
            this.doWork(modelContainer, block);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return block;
    }


    //初始化构建返回结果模型
    protected abstract T initBlock();

    //定义抽象模板
    protected abstract void doWork(ModelContainer modelContainer, T block);
}
