package com.example.app.DesignPatterns.Template.business;

import lombok.Data;

/**
 * @author tian
 * @item 2022/11/14 10:17:00
 */
public class ItemInfoBlock extends AbstractTemplateBlock<ItemInfoBlock.itemInfo> {

    @Override
    protected itemInfo initBlock() {
        return new ItemInfoBlock.itemInfo();
    }

    @Override
    protected void doWork(ModelContainer modelContainer, itemInfo block) {
        block.setItemId(123L);
        block.setItemName("测试");
    }

    @Data
    public class itemInfo {
        private Long itemId;
        private String itemName;
    }
}
