package com.example.app.DesignPatterns.Template.demo;

/**
 * @author tian
 * @item 2022/11/14 09:02:29
 */
public class TemplateTest {
    public static void main(String[] args) {
        AskForLeaveFolw.CompanyA companyA = new AskForLeaveFolw.CompanyA();
        companyA.askForLeave("tc");
        System.out.println("-----------------------------");
        System.out.println();
        AskForLeaveFolw.CompanyB companyB = new AskForLeaveFolw.CompanyB();
        companyB.askForLeave("tc");

    }
}
