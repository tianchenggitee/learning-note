package com.example.app.DesignPatterns.Template.demo;

/**
 * @author tian
 * @item 2022/11/14 08:50:54
 */
public abstract class AskForLeaveFolw {
    //一组组长直接审批
    protected abstract void FirstGroupLeader(String name);

    //二组组长部门负责审批
    protected void SencondGroupLeader(String name) {
    }

    //同事HR部门有人请假了
    private final void notify(String name) {
        System.out.println("当前有人请假了，请假人：" + name);
    }

    public void askForLeave(String name ){
        FirstGroupLeader(name);
        SencondGroupLeader(name);
        notify(name);

    }

    public static class CompanyA extends AskForLeaveFolw {
        @Override
        protected void FirstGroupLeader(String name) {
            System.out.println("companyA 组内有人请假了，请假人" + name);
        }
    }
    public static class CompanyB extends AskForLeaveFolw{

        @Override
        protected void FirstGroupLeader(String name) {
            System.out.println("companyB 组内有人请假了，请假人" + name);
        }

        @Override
        protected void SencondGroupLeader(String name) {
            System.out.println("companyB 组有人请假了，请假人" + name);
        }
    }
}

