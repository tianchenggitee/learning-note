package com.example.app.DesignPatterns.observer;

import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

/**
 * @author tian
 * @item 2022/11/05 10:31:18
 */
public class specificSubject implements Subject {
    private List<obServer> servers = new ArrayList<>();

    @Override
    public void add(obServer server) {
        servers.add(server);
    }

    @Override
    public void del(obServer server) {
        servers.remove(server);
    }

    @Override
    public void notice(String message) {
        for (obServer server : servers) {
            server.update(message);
        }
    }
}
