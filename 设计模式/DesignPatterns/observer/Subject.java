package com.example.app.DesignPatterns.observer;

import java.util.Observer;

/**
@author tian
@item  2022/11/05 10:27:57
*/public interface Subject {
    //添加订阅关系
    void add(obServer server);

    //删除订阅关系
    void del(obServer server);

    //通知订阅者
    void notice(String message);
}
