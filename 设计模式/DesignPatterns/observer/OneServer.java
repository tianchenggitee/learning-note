package com.example.app.DesignPatterns.observer;
/**
 * @author tian
 * @item 2022/11/05 11:01:43
 */
public class OneServer implements obServer {


    @Override
    public void update(String message) {
        System.out.println(message);

    }
}
