package com.example.app.DesignPatterns.observer;

/**
 * @author tian
 * @item 2022/11/05 10:34:22
 */
public interface obServer {
    //处理业务逻辑
    void update(String message);
}
