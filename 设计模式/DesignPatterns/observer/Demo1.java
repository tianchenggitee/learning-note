package com.example.app.DesignPatterns.observer;

import org.springframework.context.ApplicationListener;

import java.util.Observable;

/**
 * @author tian
 * @item 2022/11/05 11:03:09
 */
public class Demo1 {
    public static void main(String[] args) {
        specificSubject subject = new specificSubject();
        subject.add(new OneServer());
        subject.notice("我嗨了");
    }
}
