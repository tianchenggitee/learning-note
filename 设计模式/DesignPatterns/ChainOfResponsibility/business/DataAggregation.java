package com.example.app.DesignPatterns.ChainOfResponsibility.business;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author tian
 * @item 2022/11/12 13:52:40
 */
@Component
public class DataAggregation {
    @Autowired
    private SkuInfoHandler skuInfoHandler;

    @Autowired
    private ItemInfoHandler itemInfoHandler;

    public Map convertIntemDetail() throws Exception {
        Map map = new ConcurrentHashMap();
        map.put("SkuInfo", new SkuInfoHandler().doRequest("模拟数据请求"));
        map.put("ItemInfo", new ItemInfoHandler().doRequest("模拟数据请求"));

        return map;
    }

    public static void main(String[] args) throws Exception {
        DataAggregation dataAggregation = new DataAggregation();
        Map map = dataAggregation.convertIntemDetail();
        System.out.println(JSON.toJSONString(map));
    }

}
