package com.example.app.DesignPatterns.ChainOfResponsibility.business;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @author tian
 * @item 2022/11/12 13:50:11
 */
@Component
public class SkuInfoHandler extends AbsracDataHandler<SkuInfoHandler.SkuInfo> {


    @Override
    protected SkuInfo doRequest(String query) throws Exception {
        SkuInfoHandler.SkuInfo skuInfo = new SkuInfo();
        skuInfo.setSkuId(465789L);
        skuInfo.setSkuName("SKU");
        return skuInfo;
    }

    @Data
    public static class SkuInfo {
        private Long skuId;
        private String skuName;

    }
}
