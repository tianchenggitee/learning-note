package com.example.app.DesignPatterns.ChainOfResponsibility.business;

import org.apache.poi.ss.formula.functions.T;

/**
 * @author tian
 * @item 2022/11/12 13:46:02
 */
public abstract class AbsracDataHandler<T> {
    //处理模块化数据
    protected abstract T doRequest(String query) throws Exception;
}
