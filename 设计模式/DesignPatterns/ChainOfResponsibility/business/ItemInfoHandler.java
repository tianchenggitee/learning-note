package com.example.app.DesignPatterns.ChainOfResponsibility.business;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @author tian
 * @item 2022/11/12 13:47:35
 */
@Component
public class ItemInfoHandler extends AbsracDataHandler<ItemInfoHandler.ItemInfo>{
    @Override
    protected ItemInfo doRequest(String query) throws Exception {
        ItemInfoHandler.ItemInfo itemInfo = new ItemInfo();
        itemInfo.setItemId(123456L);
        itemInfo.setItmName("商品");
        return itemInfo;
    }


    @Data
    public static class ItemInfo{
        private Long itemId;
        private String itmName;
    }
}
