package com.example.app.DesignPatterns.ChainOfResponsibility.example;

/**
 * @author tian
 * @item 2022/11/12 10:09:48
 */
public class ThreeInterView extends HandlerA{

    @Override
    public void handlerRequest(Integer items) {
        if (items == 3) {
            System.out.println("第三次");
        }
    }
}
