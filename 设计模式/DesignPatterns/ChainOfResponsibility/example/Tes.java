package com.example.app.DesignPatterns.ChainOfResponsibility.example;

/**
 * @author tian
 * @item 2022/11/12 10:14:51
 */
public class Tes {
    public static void main(String[] args) {

        FirstInterView f = new FirstInterView();
        SencendInterView s = new SencendInterView();
        ThreeInterView t = new ThreeInterView();
        f.setHandler(s);
        s.setHandler(t);

        f.handlerRequest(1);
        System.out.println();

        f.handlerRequest(2);
        System.out.println();

        f.handlerRequest(3);
        System.out.println();
        String property = System.getProperty("project.name");
        System.out.println("property = " + property);
//        ConsumerContextFilter
    }
}
