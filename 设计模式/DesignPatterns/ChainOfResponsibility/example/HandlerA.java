package com.example.app.DesignPatterns.ChainOfResponsibility.example;

/**
 * @author tian
 * @item 2022/11/12 09:53:18
 */
public abstract class HandlerA {
    protected HandlerA handler;

    public void setHandler(HandlerA handler) {
        this.handler = handler;
    }

    public abstract void handlerRequest(Integer items);

//    public void doSomething() {
//        System.out.println("handler = " + handler);
//        handlerRequest(1);
//
//    }



}
