## **spring两大核心之一：Aop**  

### **1、问题**  
现在有一个问题，就是服务直接相互调用的时候，为了不埋坑，会将调用的代码try-catch住，而最终只会将调用成功的状态码返回，这样调用的一方只需要知道状态码就知道是否调用成功。  
但是如果对所有的代码块使用try-catch的工作量是不小的，有人提议加一个工具类，那如果是统计代码的执行时间呢，每一个代码块前后都要加我们的逻辑吗，这样就很冗余，对于项目来说是灾难性的。  

### **2、引出Aop**
通过上面的问题，我们引出Aop,通俗点讲就是在不改变原有的代码的情况下，在核心逻辑的前后加上我们的切面逻辑，打开扩展关闭修改，符合开闭原则，配合元注解+aop就可以解决上面的问题，在不修改源代码的基础上加一个注解。    
aop不仅仅可以再前后加入切面逻辑，还可以不执行原来的逻辑，直接执行切面的逻辑并且返回，所以他的功能还是很强大的。   

### **3、什么是Aop**  
先了解三个概念：  
joinPoint:切点，也就是切入的时间点。
pointCut:选择什么时候切入，可以理解为筛选的条件。  
Active：对逻辑的增强处理,可以选择前置处理、后置处理、异常处理、环绕处理  
举个例子：去饭店点菜,有十个菜，这十个菜就是joinPoint,那我想吃带排骨的菜，最后筛选出两个带排骨的菜红烧排骨和桥头排骨，这个过程就是pointCut,然后我们就可以在吃胡萝卜前洗手（before advice），或吃胡萝卜后买单（after advice），也可以统计吃胡萝卜的时间（around advice），这些洗手，买单，统计时间的动作都是与吃萝卜这个业务动作解藕的,都是统一写在 advice 的逻辑里.  
官方：  
JoinPoint: 程序在执行流程中经过的一个个时间点，这个时间点可以是方法调用时，或者是执行方法中异常抛出时，也可以是属性被修改时等时机，在这些时间点上你的切面代码是可以（注意是可以但未必）被注入的  

Pointcut: JoinPoints 只是切面代码可以被织入的地方，但我并不想对所有的 JoinPoint 进行织入，这就需要某些条件来筛选出那些需要被织入的 JoinPoint，Pointcut 就是通过一组规则(使用 AspectJ pointcut expression language 来描述) 来定位到匹配的 joinpoint  

Advice:  代码织入（也叫增强），Pointcut 通过其规则指定了哪些 joinpoint 可以被织入，而 Advice 则指定了这些 joinpoint 被织入（或者增强）的具体时机与逻辑，是切面代码真正被执行的地方，主要有五个织入时机  
Before Advice: 在 JoinPoints 执行前织入  
After Advice: 在 JoinPoints 执行后织入（不管是否抛出异常都会织入）  
After returning advice: 在 JoinPoints 执行正常退出后织入（抛出异常则不会被织入）  
After throwing advice: 方法执行过程中抛出异常后织入  
Around Advice: 这是所有 Advice 中最强大的，它在 JoinPoints 前后都可织入切面代码，也可以选择是否执行原有正常的逻辑，如果不执行原有流程，它甚至可以用自己的返回值代替原有的返回值，甚至抛出异常。在这些 advice 里我们就可以写入切面代码了 综上所述，切面（Aspect）我们可以认为就是 pointcut 和 advice，pointcut 指定了哪些 joinpoint 可以被织入，而 advice 则指定了在这些 joinpoint 上的代码织入时机与逻辑  
画外音：织入（weaving），将切面作用于委托类对象以创建 adviced object 的过程  


### **4、怎么实现的**
其实他的底层采用的代理来实现的，通过cglib的动态代理，通过Enhancer对象设置属性参数，创建委托类的实例，并通过继承的方式来重写委托类的非final修饰的方法(final修饰的方法不能重载)，加入我们的增强逻辑。

### **5、什么是代理、代理的分类**
代理就是当我们执行一个逻辑方法的时候，不需要真实对象自己执行，而且代理的类帮你执行。比如说我们想买房，不需要我们直接跟开发商沟通，只需要找到中介，他会帮我们筛选出适合我们的房子，并且和开发商商讨细节和价格，而我们只需要给他们相应的中介费，和交房子的房款就好了。  
代理分为静态代理和动态代理，静态代理就是通过硬编码的方式指定我们代理的对象方法，并在前后增加我们的切面逻辑。而动态代理就是我们不需要指定具体的对象方法，通过反射机制（jdk动态代理）或者继承拦截的方式（cglib动态代理）来代理对象的方法，并加入我们的切面逻辑。  

### **6、静态代理**  
        public interface Subject {
            public void request();
        }
        public class RealSubject implements Subject {
            @Override
            public void request() {
                //真实对象的买房动作
                System.out.println("卖房");
            }
        }
        public class staticProxy implements Subject {
        private RealSubject realSubject;
            //传入需要的构造参数
            public staticProxy(RealSubject realSubject) {
                this.realSubject = realSubject;
            }
            @Override
            public void request() {
                //静态代理类执行买房前后的动作
                System.out.println("卖房前");
                realSubject.request();
                System.out.println("卖房后");
         }       
      }
        public class staticTest {
        public static void main(String[] args) {
        //创建一个目标对象
        RealSubject realSubject = new RealSubject();
        //传入静态代理对象
        staticProxy staticProxy = new staticProxy(realSubject);
        //执行方法
        staticProxy.request();
            }
        }  
打印结果
![img.png](image/img.png)
静态代理:由程序员创建代理类或特定工具自动生成源代码再对其编译。在编译时已经将接口，被代理类（委托类），代理类等确定下来，在程序运行前代理类的.class文件就已经存在了。  
我们定义一个代理接口，通过真实类实现并重写代理接口来实现自己的逻辑，代理类实现代理接口通过构造器传参的方式传入真实对象，并在真实对象调用方法的前后增加自己的切面逻辑，来实现静态代理。  
缺点：顾名思义，只能通过硬编码的方式来实现代理，不够灵活  
**静态代理主要有两大劣势：**  
代理类只代理一个委托类（其实可以代理多个，但不符合单一职责原则），也就意味着如果要代理多个委托类，就要写多个代理（别忘了静态代理在编译前必须确定）  
第一点还不是致命的，再考虑这样一种场景：如果每个委托类的每个方法都要被织入同样的逻辑，比如说我要计算前文提到的每个委托类每个方法的耗时，就要在方法开始前，开始后分别织入计算时间的代码，  
那就算用代理类，它的方法也有无数这种重复的计算时间的代码。  

### **7、动态代理：jdk的动态代理**
        public interface Subject {
             public void request();
        }
        public class RealSubject implements Subject {
            @Override
            public void request() {
            //真实对象的买房动作
            System.out.println("卖房");
            }
        }
        public class jdkDynamicProxy {

        private Object target;//维护一个目标对象

        public jdkDynamicProxy(Object target) {
            this.target = target;
        }
    public Object getProxyInstance(){
        return Proxy.newProxyInstance(target.getClass().getClassLoader(), target.getClass().getInterfaces(), new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                System.out.println("卖房前");//执行逻辑的前置逻辑
                method.invoke(target, args);//执行目标对象的方法
                System.out.println("卖房后");//执行逻辑的后置逻辑
                return null;
            }
        });
    }
    }
        public class jdkDynamicTest {
            public static void main(String[] args) {
            //创建一个目标对象
            RealSubject realSubject = new RealSubject();
            System.out.println("realSubject = " + realSubject);
            //传入目标对象获取实例
            Subject subject = (Subject) new jdkDynamicProxy(realSubject).getProxyInstance();
            //调用代理方法
            subject.request();
        }
    }
打印结果：
![img.png](image/img2.png)
loader: 代理类的ClassLoader，最终读取动态生成的字节码，并转成 java.lang.Class 类的一个实例（即类），通过此实例的 newInstance() 方法就可以创建出代理的对象  
interfaces: 委托类实现的接口，JDK 动态代理要实现所有的委托类的接口  
InvocationHandler: 委托对象所有接口方法调用都会转发到 InvocationHandler.invoke()，在 invoke() 方法里我们可以加入任何需要增强的逻辑 主要是根据委托类的接口等通过反射生成的  
jdk的动态代理是通过反射机制实现的，通过反射获取接口的实现类，并在接口的实现前后增加自己的逻辑，缺点就是必须要有接口，而且反射的机制性能是相对较慢的。
JDK 动态代理是通过与委托类实现同样的接口，然后在实现的接口方法里进行增强来实现的，这就意味着如果要用 JDK 代理，委托类必须实现接口，这样的实现方式看起来有点蠢，更好的方式是什么呢，直接继承自委托类不就行了，这样委托类的逻辑不需要做任何改动，CGlib 就是这么做的

### **8、动态代理：cglib动态代理**
        public class RealSubject  {
            public void request() {
            //真实对象的买房动作
            System.out.println("卖房");
        }
    }
        public class cglibMethodInterceptor implements MethodInterceptor {
            @Override
            public Object intercept(Object obj, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
            System.out.println("卖房前");
            //注意这里是方法调用，不是反射
            Object o = methodProxy.invokeSuper(obj, args);
            System.out.println("卖房后");
            return o;

            }
        }
        public class cglibProxyTest  {
            public static void main(String[] args) {
            //创建Enhancer对象，类似于jdk动态代理的proxy 下一步是设置几个参数
            Enhancer enhancer = new Enhancer();
            //设置目标字节码文件
            enhancer.setSuperclass(RealSubject.class);
            //设置回调函数
            enhancer.setCallback(new cglibMethodInterceptor());
            //这里的create方式就是正式创建代理类
            RealSubject proxy = (RealSubject) enhancer.create();
            //调用代理类的方法
            proxy.request();
        }
cglib的动态代理是通过继承真实的代理对象，拦截父类的方法，并在方法的前后增加自己的切面逻辑，相对jdk的动态代理而言，性能比较好，而且不需要代理接口。  
JDK 动态代理使用 Proxy 来创建代理类，增强逻辑写在 InvocationHandler.invoke() 里，CGlib 动态代理也提供了类似的  Enhance 类，增强逻辑写在 
MethodInterceptor.intercept() 中，也就是说所有委托类的非 final 方法都会被方法拦截器拦截，在说它的原理之前首先来看看它怎么用的  
 

### **9、spring为什么使用cglib动态代理**  
CGLIB并不要求委托类实现任何接口，而且 CGLIB 是高效的代码生成包，底层依靠 ASM（开源的 java 字节码编辑类库）操作字节码实现的，   
性能比 JDK 强，所以 Spring AOP 最终使用了 CGlib 来生成动态代理。于反射的效率比较低，所以 CGlib 采用了FastClass 的机制来实现对被拦截方法的调用。  
FastClass 机制就是对一个类的方法建立索引，通过索引来直接调用相应的方法  

### **10、小结**  
AOP 是 Spring 一个非常重要的特性，通过切面编程有效地实现了不同模块相同行为的统一管理，也与业务逻辑实现了有效解藕，善用 AOP 有时候能起到出奇制胜的效果，  
举一个例子，我们业务中有这样的一个需求，需要在不同模块中一些核心逻辑执行前过一遍风控，风控通过了，这些核心逻辑才能执行，怎么实现呢，你当然可以统一封装一  
个风控工具类，然后在这些核心逻辑执行前插入风控工具类的代码，但这样的话核心逻辑与非核心逻辑（风控，事务等）就藕合在一起了，更好的方式显然应该用 AOP，使用  
注解 + AOP 的方式，将这些非核心逻辑解藕到切面中执行，让代码的可维护性大大提高了。  




