## **spring两大核心之一：Ioc**

### **IoC 容器**  
#### **Spring如何设计容器的**  
使用ApplicationContext,他是BeanFactory的子类，更好的补充并实现了BeanFactory。使用ApplicationContext有两个具体实现的子类
![img.png](image/img.png)
ClassPathXmlApplicationContext :从class path中加载配置文件  
FileSystemXmlApplicationContext:从本地文件中加载配置文件(不常用)
通过点击ClassPathXmlApplicationContext的时候，发现他并不是直接集成ApplicationContext,而是有多层依赖关系，  
每一次依赖关系都是子类对父类的实现与补充，最上层回到了BeanFatory,所以BeanFatory很重要。
![img.png](image/img2.png)

#### **何为控制，控制的是什么**  
是bean创建、管理的权利，bean的生命周期  
#### **何为反转，反转了什么**
交给容器进行管理，而不是自己，这就是反转.由之前的自己主动创建对象，变成现在被动接收别人给我们的对象的过程，这就是反转。     

### **DI 依赖注入**    
#### **依赖什么**  
程序运行依赖外部的数据，提供程序内对象所需的数据、资源
#### **注入什么**  
配置文件把资源从外部加载的资源、对象、数据注入在程序内的对象，维护了内部程序对象之间的依赖关系  
也就是说Ioc是一种思想，而DI是一种实现，控制反转就是通过依赖注入来实现的。

#### **为什么用Ioc,有什么好处**  
答案就是解耦，把对象之间的关系用配置文件来管理，交给Ioc容器，在项目里底层都是各个对象之间的依赖关系，  
如果他们之间相互依赖，一个对象出现问题，整个项目都会出现问题，而通过解耦这种思想来避免这个问题。


### **总结**  
Ioc就是控制翻转，什么是控制翻转，就是把创建对象的实例交给spring容器，省略了我们自己new的过程，这样做有什么好处呢    
解耦，当我们需要的时候，直接从容器里面拿就好了，只需要一个自动注入的注解，就可以拿到实例，编写各个实例之间的业务代码    
简直不要太爽。  
#### **我们最后再来体会一下用 Spring 创建对象的过程:**  
通过 ApplicationContext 这个 IoC 容器的入口，用它的两个具体的实现子类，从 class path 或者 file path 中读取数据，用 getBean() 获取具体的 bean instance。  
那使用 Spring 到底省略了我们什么工作？  
答：new 的过程。把 new 的过程交给第三方来创建、管理，这就是「解藕」  
![img.png](image/img3.png)
