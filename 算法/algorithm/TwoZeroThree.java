package com.seentao.algorithm;

import java.util.LinkedList;

/**
 * @author tian
 * @item 2021/12/21 20:48:29
 * 203. 移除链表元素
 * 给你一个链表的头节点 head 和一个整数 val
 * ，请你删除链表中所有满足 Node.val == val 的节点，
 * 并返回 新的头节点 。
 * 输入：head = [1,2,6,3,4,5,6], val = 6
 * 输出：[1,2,3,4,5]
 */
public class TwoZeroThree {
    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(4);
        head.next.next.next.next = new ListNode(5);
        head.next.next.next.next.next = new ListNode(6);
        head.next.next.next.next.next.next = new ListNode(6);
        ListNode resilt = twoZeroThree(head, 6);
        System.out.println(resilt);


    }

    private static ListNode twoZeroThree(ListNode head, int i) {
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        ListNode prev = dummy;
        while (head != null ) {
            if (head.val == i) {
                prev.next = head.next;
                head = head.next;
            } else {
                prev = head;
                head = head.next;
            }
        }
        return dummy.next;

    }

}
