package com.seentao.algorithm;

/**
 * @author tian
 * @item 2021/12/20 20:38:05
 * 283. 移动零
 * 给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。
 * 输入: [0,1,0,3,12]
 * 输出: [1,3,12,0,0]
 */
public class TwoEightThree {
    public static void main(String[] args) {
        int[] nums = {1, 0, 2, 0, 3, 0};
        nums = findData(nums);
        for (int num : nums) {
            System.out.println(num);
        }

    }

    private static int[] findData(int[] nums) {
        int index = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != 0) {
                nums[index] = nums[i];
                index++;

            }
        }
        for (int i = index; i < nums.length; i++) {
            nums[i] = 0;
        }

        return nums;
    }
}
