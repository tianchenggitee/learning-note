package com.seentao.algorithm;

import cn.hutool.core.convert.Convert;

import java.util.Stack;

/**
 * @author tian
 * @item 2021/12/24 16:20:26
 *20. 有效的括号
 * 给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。
 * 有效字符串需满足：
 * 左括号必须用相同类型的右括号闭合。
 * 左括号必须以正确的顺序闭合。
 * 输入：s = "()[]{}"
 * 输出：true
 */
public class TwoZero {
    public static void main(String[] args) {
        String str = "";
        Boolean aBoolean = twoZero(str);
        System.out.println(aBoolean);

    }

    private static Boolean twoZero(String str) {
        Stack<Character> stack = new Stack<>();
        for (char c : str.toCharArray()) {
            if (c == '(') {
                stack.push(')');
            } else if (c == '[') {
                stack.push(']');
            } else if (c == '{') {
                stack.push('}');
            } else if (stack.isEmpty() || c != stack.pop()) {
                return false;
            }
        }
        return stack.isEmpty();
    }

}
