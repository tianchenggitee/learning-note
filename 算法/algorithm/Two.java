package com.seentao.algorithm;

/**
 * 2:两数相加
 * 给你两个 非空 的链表，表示两个非负的整数。它们每位数字都是按照 逆序 的方式存储的，并且每个节点只能存储 一位 数字。
 *
 * 请你将两个数相加，并以相同形式返回一个表示和的链表。
 *
 * 你可以假设除了数字 0 之外，这两个数都不会以 0开头。
 *输入：l1 = [2,4,3], l2 = [5,6,4]
 * 输出：[7,0,8]
 * 解释：342 + 465 = 807.
 */

public class Two {
    public static void main(String[] args) {
        Two l1 = new Two(1);
        Two l1_2 = new Two(2);
        Two l1_3 = new Two(9);
        l1.next = l1_2;
        l1_2.next = l1_3;
       l1.toString();

        Two l2 = new Two(9);
        l2.next = new Two(5);
        l2.next.next = new Two(2);
       l2.toString();

        Solution solu = new Solution();
        Two l3 = solu.addTwoNumbers(l1,l2);
        l3.toString();
    }
    int val;
    Two next;

    Two() {
    }

    Two(int val) {
        this.val = val;
    }

    Two(int val, Two next) {
        this.val = val;
        this.next = next;
    }

    static class Solution {
        public Two addTwoNumbers(Two l1, Two l2) {
            Two head = new Two(0);
            Two p = l1, q = l2, curr = head;
            int carry = 0;
            while (p != null || q != null) {
                int x = p == null ? 0 : p.val;
                int y = q == null ? 0 : q.val;
                int sum = x + y + carry;
                carry = sum / 10;
                curr.next = new Two(sum % 10);
                curr = curr.next;
                if (p != null) {
                    p = p.next;
                }
                if (q != null) {
                    q = q.next;
                }
            }
            if (carry > 0) {
                curr.next = new Two(carry);
            }


            return head.next;
        }
    }
}