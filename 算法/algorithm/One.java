package com.seentao.algorithm;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * @author tian
 * @item 2021/07/07 17:53:03
 * 1:两数之和
 * 给定一个整数数组 nums 和一个整数目标值 target，
 * 请你在该数组中找出 和为目标值 target  的那 两个 整数，
 * 并返回它们的数组下标。
 * 示例 1：
 *
 * 输入：nums = [2,7,11,15], target = 9
 * 输出：[0,1]
 * 解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。
 */
public class One {
    public static void main(String[] args) {
        int nums[] = {1, 2, 3, 4, 5};
        int target = 6;
        One foo = new One();
        int[] ints = foo.twoSum(nums, target);
        for (int anInt : ints) {
            System.out.println("anInt = " + anInt);
        }


    }

    public int[] twoSum(int[] nums, int target) {
        int[] num = new int[2];
        Map<Integer, Integer> map = Maps.newHashMap();
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])) {
                num[0] = i;
                num[1] = map.get(nums[i]);
                return num;
            } else {
                map.put(target - nums[i], i);
            }
        }
            return nums;
        }
}
