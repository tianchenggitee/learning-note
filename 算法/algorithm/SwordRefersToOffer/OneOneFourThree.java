package com.seentao.algorithm.SwordRefersToOffer;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author tian
 *
 *  @item 2022/12/06 10:40:44
 *  给定两个字符串 text1 和 text2，返回这两个字符串的最长 公共子序列 的长度。如果不存在 公共子序列 ，返回 0 。
 *
 * 一个字符串的 子序列 是指这样一个新的字符串：它是由原字符串在不改变字符的相对顺序的情况下删除某些字符（也可以不删除任何字符）后组成的新字符串。
 *
 * 例如，"ace" 是 "abcde" 的子序列，但 "aec" 不是 "abcde" 的子序列。
 * 两个字符串的 公共子序列 是这两个字符串所共同拥有的子序列。
 */

public class OneOneFourThree {
    //时间复杂度为O(m * n * maxLength)，空间复杂度是O(1)。
    public static String oneonefourthree(String str1, String str2) {
        int count = 0;
        if (str1 == null || str1.equals("") || str2 == null || str2.equals("")) {
            return "-1";
        }
        char[] chars1 = str1.toCharArray();
        char[] chars2 = str2.toCharArray();
        //出现的最大公共子串的长度
        int maxLenth = 0;
        //出现最大公共子串在str1最后一个字符出现的index
        int maxEndindex = 0;
        for (int i = 0; i < chars1.length; i++) {
            for (int j = 0; j < chars2.length; j++) {
                count++;

                if (chars1[i] == chars2[j]) {
                    int tempIndex1 = i;
                    int tempIndex2 = j;
                    int tempMax = 0;

                    while (tempIndex1 < chars1.length && tempIndex2 < chars2.length && chars1[tempIndex1] == chars2[tempIndex2]) {
                        tempIndex1++;
                        tempIndex2++;
                        tempMax++;
                    }
                    if (tempMax > maxLenth) {
                        maxLenth = tempMax;
                        maxEndindex = tempIndex1;
                    }


                }

            }
        }
        System.out.println("count = " + count);
        if (maxLenth == 0) {
            return "-1";
        } else {
            return str1.substring(maxEndindex - maxLenth, maxEndindex);
        }

    }

    public static String secondsolution(String str1, String str2) {
        AtomicInteger count = new AtomicInteger(0);
        if (str1 == null || str1.equals("") || str2 == null || str2.equals("")) {
            return "-1";
        }
        //将String转为char数组
        char[] chars1 = str1.toCharArray();
        char[] chars2 = str2.toCharArray();
        //构建map 即char->index 如果相同char index 有多个
        // 则为index1，index2 构建这个是为了方便在chars1遍历的是O(1) 查找char在chars2的index列表
        Map<Character, String> map = new ConcurrentHashMap<>();
        for (int i = 0; i < chars2.length; i++) {
            if (map.containsKey(chars2[i])) {
                map.put(chars2[i], map.get(chars2[i]) + "," + i);
            } else {
                map.put(chars2[i], "" + i);
            }
        }
        int max = 0;//出现最大公共长度
        int maxIndex = 0;//最大长度在chars1中的起始处索引

        for (int i = 0; i < chars1.length; i++) {

            if (map.containsKey(chars1[i])) {//说明cahrs2中有着字符 需要计算

                String indexArray = map.get(chars1[i]);
                List<Integer> indexList = Stream.of(indexArray.split(",")).map(Integer::parseInt).collect(Collectors.toList());

                for (Integer index : indexList) {
                    count.incrementAndGet();
                    int tempIndex1 = i;//用于记录标记在chars1的遍历序号
                    int tempCommonLenth = 0;//用于标记从index起相同长度
                    while (index < chars2.length && tempIndex1 < chars1.length && chars1[tempIndex1] == chars2[index]) {
                        tempCommonLenth++;
                        tempIndex1++;
                        index++;
                    }
                    if (tempCommonLenth > max) {
                        max = tempCommonLenth;
                        maxIndex = i;
                    }

                }
            }

        }
        System.out.println("count:" + count);
        if (max == 0) {
            return "-1";
        } else {
            return str1.substring(maxIndex, maxIndex + max);
        }


    }

    public static void main(String[] args) {
//        System.out.println(oneonefourthree("abcde","abd"));
//        System.out.println(secondsolution("abcde","abd"));
        int[][] db = new int[1][2];
        System.out.println(db.toString());
    }

}
