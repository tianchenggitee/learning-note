package com.seentao.algorithm.SwordRefersToOffer;

import com.seentao.algorithm.ListNode;

import java.util.Stack;

/**
 * @author tian
 * 剑指 Offer 06. 从尾到头打印链表\
 * 输入一个链表的头节点，从尾到头反过来返回每个节点的值（用数组返回）。
 *
 *
 * 示例 1：
 *
 * 输入：head = [1,3,2]
 * 输出：[2,3,1]
 * @item 2022/03/29 13:54:29
 */
public class Six {
    public static void main(String[] args) {
        ListNode listNode = new ListNode(1);
        listNode.next = new ListNode(3);
        listNode.next.next = new ListNode(2);
        int[] ints = reversePrint(listNode);
        System.out.println("ints = " + ints);

    }

    public static int[] reversePrint(ListNode head) {
        ListNode curr = head;
        Stack<Integer> stack = new Stack<>();
        while (curr != null) {
            stack.push(curr.val);
            curr = curr.next;
        }
        int[] nums = new int[stack.size()];
        for (int i = 0; i < nums.length; i++) {
            nums[i] = stack.pop();
        }
        return nums;
    }

}
