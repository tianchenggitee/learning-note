package com.seentao.algorithm;

/**
 * @author tian
 * @item 2021/12/20 20:16:34
 * 485. 最大连续 1 的个数
 * 给定一个二进制数组， 计算其中最大连续 1 的个数。
 * 输入：[1,1,0,1,1,1]
 * 输出：3
 * 解释：开头的两位和最后的三位都是连续 1 ，所以最大连续 1 的个数是 3.
 */
public class FourEightFive {
    public static void main(String[] args) {
        int[] nums = {1, 1, 2, 1, 1, 1};

        int data = findData(nums);
        System.out.println(data);

    }

    private static int findData(int[] nums) {
        int count = 0, result = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 1) {
                count++;
            } else {
                result = Math.max(result, count);
                 count = 0;
            }
        }
        return Math.max(result, count);
    }
}
