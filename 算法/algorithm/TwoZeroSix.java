package com.seentao.algorithm;

import java.util.List;

/**
 * @author tian
 * @item 2021/12/23 19:52:32
 * 206. 反转链表
 *给你单链表的头节点 head ，请你反转链表，并返回反转后的链表。
 * 输入：head = [1,2,3,4,5]
 * 输出：[5,4,3,2,1]
 */
public class TwoZeroSix {
    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
//        head.next.next.next = new ListNode(4);
//        head.next.next.next.next = new ListNode(5);
//        head.next.next.next.next.next = new ListNode(6);
        twoZeroSix(head);
    }

    private static void twoZeroSix(ListNode head) {
        ListNode prev = null;//前一个指针
        ListNode curr = head;//当前指针
        //每次循环 都将当前节点指向前面的节点 然后当前节点和前面的节点后移
        while (curr!=null){
        ListNode temp = curr.next;//临时变量保存当前指针的下一个节点
        curr.next = prev;//当前节点指向他前面的节点
        prev = curr;//前节点后移
        curr = temp;//当前节点后移
        }
        System.out.println(prev);
    }
}
